# Asqatasun `ZZ-version-ZZ`

`Dockerfile` and `docker-compose-.yml` files that are available:

- [Asqatasun `ZZ-version-ZZ` **API**](api-ZZ-version-ZZYY-extra-path-YY)
- [Asqatasun `ZZ-version-ZZ` **Webapp**](webapp-ZZ-version-ZZYY-extra-path-YY)
- [Asqatasun `ZZ-version-ZZ` **API** + **Webapp**](all-ZZ-version-ZZYY-extra-path-YY)

> ⚠️ Created for testing purpose only, no security has been made for production. ⚠️

## Prerequisites

- [Docker](https://docs.docker.com/engine/install/) `19.03.0` (at least) is required
- [Docker Compose](https://docs.docker.com/compose/install/) `1.27.0` (at least) is required

## Ports, URL and credentials (user/password)

| Service  | Port | URL                                     | User                         | Password                        |
|----------|------|-----------------------------------------|------------------------------|---------------------------------|
| Database | 3306 | `jdbc:mysql://localhost:3306/asqatasun` | `asqatasunDatabaseUserLogin` | `asqatasunDatabaseUserP4ssword` |
| API      | 8081 | `http://localhost:8081`                 | `admin@asqatasun.org`        | `myAsqaPassword`                |
| Webapp   | 8080 | `http://localhost:8080`                 | `admin@asqatasun.org`        | `myAsqaPassword`                |

Tip:
if you copy `.env.dist` file to `.env` file,
you can change **port** numbers, **IP** adresses and **database** credentials.


## Software versions

- Asqatasun **ZZ-version-ZZ**
- Geckodriver **ZZ-geckodriver-version-ZZ**
- Firefox **ZZ-firefox-version-ZZ**

# Asqatasun `5-SNAPSHOT`

`Dockerfile` and `docker-compose-.yml` files that are available:

- [Asqatasun `5-SNAPSHOT` **API**](api-5-SNAPSHOT_ubuntu-18.04)
- [Asqatasun `5-SNAPSHOT` **Webapp**](webapp-5-SNAPSHOT_ubuntu-18.04)
- [Asqatasun `5-SNAPSHOT` **API** + **Webapp**](all-5-SNAPSHOT_ubuntu-18.04)

> ⚠️ Created for testing purpose only, no security has been made for production. ⚠️

## Context of use

Uses **SNAPSHOT** version of Asqatasun which is built by Gitlab CI every time source code is modified.

Each **SNAPSHOT** build can introduce new bugs, disable features or even stop working at all.

It's should be used with full knowledge of this point and only used
to test new features or check that a bug is now fixed for the next version of Asqatasun.

## Prerequisites

- [Docker](https://docs.docker.com/engine/install/) `19.03.0` (at least) is required
- [Docker Compose](https://docs.docker.com/compose/install/) `1.27.0` (at least) is required

## Ports, URL and credentials (user/password)

| Service  | Port | URL                                     | User                         | Password                        |
|----------|------|-----------------------------------------|------------------------------|---------------------------------|
| Database | 3306 | `jdbc:mysql://localhost:3306/asqatasun` | `asqatasunDatabaseUserLogin` | `asqatasunDatabaseUserP4ssword` |
| API      | 8081 | `http://localhost:8081`                 | `admin@asqatasun.org`        | `myAsqaPassword`                |
| Webapp   | 8080 | `http://localhost:8080`                 | `admin@asqatasun.org`        | `myAsqaPassword`                |

Tip:
if you copy `.env.dist` file to `.env` file,
you can change **port** numbers, **IP** adresses and **database** credentials.

## Software versions

- Asqatasun **5-SNAPSHOT**
- Geckodriver **0.30.0**
- Firefox **91.4.1esr**

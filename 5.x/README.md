# Using Docker - Asqatasun 5.x

> ⚠️ Created for testing purpose only, no security has been made for production. ⚠️

## Prerequisites

- [Docker](https://docs.docker.com/engine/install/) `19.03.0` (at least) is required
- [Docker Compose](https://docs.docker.com/compose/install/) `1.27.0` (at least) is required

## Ports, URL and credentials (user/password)

| Service  | Port | URL                                     | User                         | Password                        |
|----------|------|-----------------------------------------|------------------------------|---------------------------------|
| Database | 3306 | `jdbc:mysql://localhost:3306/asqatasun` | `asqatasunDatabaseUserLogin` | `asqatasunDatabaseUserP4ssword` |
| API      | 8081 | `http://localhost:8081`                 | `admin@asqatasun.org`        | `myAsqaPassword`                |
| Webapp   | 8080 | `http://localhost:8080`                 | `admin@asqatasun.org`        | `myAsqaPassword`                |

Tip:
if you copy `.env.dist` file to `.env` file,
you can change **port** numbers, **IP** adresses and **database** credentials.

## Choose version of Asqatasun

Choose the version of Asqatasun you want to start: `5.<minor>.<patch>`

```shell
git clone https://gitlab.com/asqatasun/asqatasun-docker
cd asqatasun-docker

# for Asqatasun Webapp 5.<minor>.<patch>
cd 5.x/5.<minor>.y/5.<minor>.<patch>/webapp(...)

# for Asqatasun API 5.<minor>.<patch>
cd 5.x/5.<minor>.y/5.<minor>.<patch>/api(...)

# for Asqatasun API + Webapp 5.<minor>.<patch>
cd 5.x/5.<minor>.y/5.<minor>.<patch>/all(...)
```

## Launch Asqatasun

- Build docker images
- Launch Asqatasun

### Option 1: uses same database, if it already exists

```shell
# build Docker image + launch Asqatasun and database (uses same database, if it already exists)
docker-compose up  --build
```

### Option 2: uses a new database

```shell
# build Docker image + launch Asqatasun and a new database
docker-compose rm -fsv
docker-compose up  --build
```

### Option 3: uses a new database and rebuild Docker images

Recommended option for **SNAPSHOT** versions of Asqatasun that can be updated at any time.

```shell
# re-build Docker image + launch Asqatasun and a new database
docker-compose rm -fsv
docker-compose build --no-cache
docker-compose up
```

## Play with Asqatasun webapp

- In your browser, go to `http://127.0.0.1:8080/`
- Use this user and this password to log in:
  - `admin@asqatasun.org`
  - `myAsqaPassword`

## Play with Asqatasun API

- In your browser: `http://127.0.0.1:8081/`  (API documentation and **Swagger** playground)
- Use this user and this password to log in:
  - `admin@asqatasun.org`
  - `myAsqaPassword`

You can refer to [Asqatasun API documentation](https://doc.asqatasun.org/v5/en/Developer/API/)
for full usage and tips on how to use it.
